# Todo

## jobs

- [x] setup psql

- [x] connect to the psql

- [] using sqlx instead of database/sql

- [] display templates

- [x] create models structures

- [] Authentication (session auth)

- [x] Serving static files

- [] CRUC functions for Authentiation (using session auth)

- [] routes

- [] write a README file

- [] comments

- [] convention

- [] make file

- [] using golint for linting the project

## in future

- [] JWT Auth

- [] validation

- [] Docker

- [] logging

- [] error handling

- [] test

- [] framework

- [] CI / CD with gitlab

## Serving Static Files

We can use the multiplexer to serve static files.
look at the code below

```go
files := http.FileServer(http.Dir("/public"))
```

to do this we use FileServer function to create a handler that will serve files from a given directories .

then we pass the handler to the Handle function of multiplexer .

```go
mux.Handle("/static/", http.StripPrefix("/static/", files))
```

In this code you're telling the server that for all request URLs starting with **/static/** ..
bia on **/static/** ro az URL delete kon va baad look for a file with the name starting at the **public**
directory ...

**for example** :

Request URL : _http://localhost/static/css/bootstrap.main.css_

the server will loop for the file :

_application-root/css/bootstrap.main.css_
