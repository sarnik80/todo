package main

import (
	"flag"
	"fmt"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"gitlab.com/sarnik80/todo/api"
	"gitlab.com/sarnik80/todo/consts"
)

var (
	DB  *sqlx.DB
	err error
)

func init() {

	DB, err = sqlx.Open("postgres", consts.Dsn)

	if err != nil {

		fmt.Println(err)
	}
}

func main() {

	listenAdd := flag.String("listenaddr", ":8080", "the server address")
	flag.Parse()

	server := api.NewServer(*listenAdd, DB)
	server.Start()

}
