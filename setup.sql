create table todos (

        id  serial primary key,

        content text ,

        title  varchar(255) not null ,

        completed boolean default false , 

        user_id  integer references users(id) not null 
);


create table users (

    id  serial primary key,

    username  varchar(255) not null,

    password  varchar(255) not null

);



create table sessions (

    id serial primary key ,

    session_id  varchar(255) ,

    activity    date not null default current_date , 

    user_id integer  references users(id) not null 

);



ALTER TABLE "todos" ADD FOREIGN KEY ("user_id") REFERENCES "users" ("id");

ALTER TABLE "sessions" ADD FOREIGN KEY ("user_id") REFERENCES "users" ("id");