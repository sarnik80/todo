package api

import (
	"net/http"

	"github.com/gorilla/mux"
	"github.com/jmoiron/sqlx"
)

//Server Sructure
type Server struct {
	listenAddr string
	store      *sqlx.DB
}

//NewServer is Constructor
func NewServer(listenaddress string, storege *sqlx.DB) *Server {

	return &Server{
		listenAddr: listenaddress,
		store:      storege,
	}
}

/*Start function boots the server
 */
func (s *Server) Start() {

	//  we can add handlers in this section

	router := mux.NewRouter()

	// Handle static files
	files := http.FileServer(http.Dir("/public"))
	router.PathPrefix("/static/").Handler(http.StripPrefix("/static/", files))

	router.HandleFunc("/", s.getTodos).Methods(http.MethodGet)

	http.ListenAndServe(s.listenAddr, router)
}
