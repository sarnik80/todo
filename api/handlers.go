package api

import (
	"fmt"
	"net/http"

	"gitlab.com/sarnik80/todo/types"
)

/*

	example

	funct (s *Server) HandleFoo(w http.ResponseWriter , r *http.Request) {


	}

*/

func (s *Server) getTodos(w http.ResponseWriter, r *http.Request) {

	var todos []types.Todo

	err := s.store.Select(&todos, "select id , content , title , completed from todos")

	if err != nil {
		fmt.Println(err)
	}

	fmt.Println(todos)

}
