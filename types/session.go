package types

import "time"

type Session struct {
	ID        int       `db:"id"`
	SessionID string    `db:"session_id"`
	Activity  time.Time `db:"activity"`
	User      *User
}
