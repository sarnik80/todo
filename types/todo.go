package types

//Todo items for rach user
type Todo struct {
	ID        int    `db:"id"`
	Content   string `db:"content"`
	Title     string `db:"title"`
	Completed bool   `db:"completed"`
	User      *User
}
