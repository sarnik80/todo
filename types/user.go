package types

//User structure deine our clients features
type User struct {
	ID        int    `db:"id"`
	Username  string `db:"username"`
	Password  string `db:"password"`
	TodoItems []Todo
}

//  user validation can be added
